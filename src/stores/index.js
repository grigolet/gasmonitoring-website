import values from "./values"
import rules from "./rules"
export default {
  values,
  rules
}
