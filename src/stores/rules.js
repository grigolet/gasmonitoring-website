import { observable } from "mobx"

export default observable([
  {
    link: "dip://dip/ALICE/GCS/ALICPV/Mixer",
    value: "Line1Ratio(L1CompRatioAS)",
    rule: value =>
      value > 80 ? "bg-green-dark text-white" : "bg-red-dark text-white"
  }
])
