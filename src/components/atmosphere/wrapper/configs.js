export default {
  url: "https://bcast.web.cern.ch/connect/broadcast",
  contentType: "application/json",
  transport: "websocket",
  fallbackTransport: "long-polling",
  enableXDR: true,
  onOpen: response => {
    console.log("Socket opened")
    console.log(response)
  },
  onClose: response => {
    console.log("Socket closed")
  },
  onTransportFailure: response => {
    console.error(response)
  },
  onError: response => {
    console.error("Error")
    console.error(response)
  },
  onMessage: response => {
    console.log("Got message ", response.responseBody)
  },
  onReconnect: (request, response) => {
    console.log("Reconnected")
    console.log(request, response)
  }
}
