import React from "react"
import { observer, inject } from "mobx-react"
import { autorun } from "mobx"
import atmosphere from "atmosphere.js"
import configs from "./configs"

class AtmosphereWrapper extends React.Component {
  componentDidMount() {
    this.socket = atmosphere
    this.subscribed = false
    this.request = {
      ...configs,
      onMessage: this._onMessage,
      logLevel: "info"
    }
    this.subSocket = this.socket.subscribe(this.request, () => {
      this.subscribed = true
      this._addSubscriptions = autorun(() => {
        if (this.subscribed) {
          const links = this.props.values.map(value => value.link)
          if (links.length) {
            this.subSocket.push(JSON.stringify({ add: links }))
          }
        }
      })
    })
  }

  componentWillUnmount() {
    this.socket.unsubscribe()
  }

  _onMessage = response => {
    const res = JSON.parse(response.responseBody)
    const values = res.value
    const link = res.description || res.name
    if (values && link) {
      const keys = Object.keys(values)
      if (keys) {
        const storeIndex = this.props.values.findIndex(el => el.link === link)
        keys.map(key =>
          this.props.values[storeIndex].values.set(key, values[key])
        )
      }
    }
  }

  render() {
    return this.props.children
  }
}

export default inject("values")(observer(AtmosphereWrapper))
