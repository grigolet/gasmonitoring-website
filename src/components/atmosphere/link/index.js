import React from "react"
import { observer, inject } from "mobx-react"
import { observable, toJS } from "mobx"
import { AtmosphereItem } from "../index"

class AtmosphereLink extends React.Component {
  constructor(props) {
    super(props)
    this.renderChildren = this.renderChildren.bind(this)
  }
  index = observable.box(-1)

  componentDidMount() {
    const index = this.props.values.findIndex(
      value => value.link === this.props.link
    )
    if (index === -1) {
      this.props.values.push({
        values: observable(new Map()),
        link: this.props.link
      })
    }
    this.index = index
  }

  renderChildren() {
    return React.Children.map(this.props.children, child => {
      if (child.type === "tr") {
        return (
          <tr>
            {React.Children.map(child.props.children, element => {
              if (element.props.children.type === AtmosphereItem) {
                const el = React.cloneElement(element.props.children, {
                  link: this.props.link
                })
                return <td>{el}</td>
              }
              return element
            })}
          </tr>
        )
      }
      return child
    })
  }

  render() {
    toJS(this.props.values)
    return this.renderChildren()
  }
}

export default inject("values")(observer(AtmosphereLink))
