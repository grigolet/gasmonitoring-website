import React from "react"
import { observer, inject } from "mobx-react"

const MqttItem = props => {
  const linkIndex = props.values.findIndex(el => el.link === props.link)
  const ruleItem = props.rules.find(el => {
    return el.link === props.link && el.value === props.value
  })
  if (linkIndex !== -1) {
    const value = props.values[linkIndex].values.get(props.value)
    if (value) {
      const ruleStyling =
        ruleItem && ruleItem.rule ? ruleItem.rule(value) : null
      return <span className={ruleStyling ? ruleStyling : null}>{value}</span>
    }
  }
  if (props.missingMessage) {
    return props.missingMessage
  }
  return null
}

export default inject("values", "rules")(observer(MqttItem))
