import React from "react"
import { observer, inject } from "mobx-react"
import {} from "mobx"
import mqtt from "mqtt"
import configs from "./configs"

class MqttWrapper extends React.Component {
  componentDidMount() {
    const urlConnection = `mqtt://${configs.url}:${configs.port}`
    this.client = mqtt.connect(urlConnection)
    // this.client.subscribe("#")
    this.props.values.map(value => {
      console.log("Subscribed to ", value.link)
      return this.client.subscribe(value.link)
    })
    this.client.on("message", this._onMessage)
  }

  componentWillUnmount() {
    this.client.end()
  }

  _onMessage = (link, message) => {
    console.log("Link: ", link)
    const res = JSON.parse(message)
    console.log("Message: ", res)
    const values = res.value
    if (values && link) {
      const keys = Object.keys(values)
      if (keys) {
        const storeIndex = this.props.values.findIndex(el => el.link === link)
        keys.map(key =>
          this.props.values[storeIndex].values.set(key, values[key])
        )
      }
    }
  }

  render() {
    return this.props.children
  }
}

export default inject("values")(observer(MqttWrapper))
