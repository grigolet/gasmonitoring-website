import React from "react"
import { MqttWrapper as MW, MqttLink as ML, MqttItem as MI } from "../../mqtt"

const NonLHC = () => (
  <div>
    <h2>Non LHC page</h2>
    <h3>CLOUD</h3>
    <MW>
      <table>
        <thead>
          <tr>
            <th />
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <ML link="dip/ALICE/GCS/ALICPV/Mixer">
            <tr>
              <td>Value: Line1Ratio(L1CompRatioAS)</td>
              <td>
                <MI value="Line1Ratio(L1CompRatioAS)" />
              </td>
            </tr>
            <tr>
              <td>Value: O2(L3Sr142O2AS</td>
              <td>
                <MI value="O2(L3Sr142O2AS)" />
              </td>
            </tr>
            <tr>
              <td>Value: Line2InputPressure(PT1203</td>
              <td>
                <MI value="Line2InputPressure(PT1203)" />
              </td>
            </tr>
            <tr>
              <td>Value: OutPressure(PT1009</td>
              <td>
                <MI value="OutPressure(PT1009)" />
              </td>
            </tr>
            <tr>
              <td>Value: State(StepperWS</td>
              <td>
                <MI value="State(StepperWS)" />
              </td>
            </tr>
            <tr>
              <td>Value: TotalFlow(TotalFlowAS</td>
              <td>
                <MI value="TotalFlow(TotalFlowAS)" />
              </td>
            </tr>
            <tr>
              <td>Value: Line2Ratio(L2CompRatioAS</td>
              <td>
                <MI value="Line2Ratio(L2CompRatioAS)" />
              </td>
            </tr>
          </ML>
        </tbody>
      </table>
      <h4>Alice MIXER</h4>
      <table>
        <thead>
          <tr>
            <th />
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <ML link="dip/ALICE/GCS/ALIHMP/Mixer">
            <tr>
              <td>Value: Line1Ratio(L1CompRatioAS)</td>
              <td>
                <MI value="Line1Ratio(L1CompRatioAS)" />
              </td>
            </tr>
            <tr>
              <td>Value: O2(L3Sr142O2AS)</td>
              <td>
                <MI value="O2(L3Sr142O2AS)" />
              </td>
            </tr>
            <tr>
              <td>Value: Line2InputPressure(PT1203)</td>
              <td>
                <MI value="Line2InputPressure(PT1203)" />
              </td>
            </tr>
            <tr>
              <td>Value: OutPressure(PT1009)</td>
              <td>
                <MI value="OutPressure(PT1009)" />
              </td>
            </tr>
            <tr>
              <td>Value: State(StepperWS)</td>
              <td>
                <MI value="State(StepperWS)" />
              </td>
            </tr>
            <tr>
              <td>Value: TotalFlow(TotalFlowAS)</td>
              <td>
                <MI value="TotalFlow(TotalFlowAS)" />
              </td>
            </tr>
            <tr>
              <td>Value: Line2Ratio(L2CompRatioAS)</td>
              <td>
                <MI value="Line2Ratio(L2CompRatioAS)" />
              </td>
            </tr>
          </ML>
        </tbody>
      </table>
    </MW>
  </div>
)

export default NonLHC
