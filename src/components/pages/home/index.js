import React from "react"
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import LHC from "../lhc"
import NonLHC from "../nonlhc"

const Home = () => (
  <div>
    <h1>GCS system status</h1>
    <Router>
      <div>
        <h3>
          <Link to="/lhc">LHC</Link>
        </h3>
        <h3>
          <Link to="/nonlhc">non LHC</Link>
        </h3>
        <Route path="/lhc" component={LHC} />
        <Route path="/nonlhc" component={NonLHC} />
      </div>
    </Router>
  </div>
)

export default Home
