import React, { Component } from "react"
import { Provider } from "mobx-react"
import { BrowserRouter as Router, Route } from "react-router-dom"
import "./styles/index.css"
import stores from "./stores"
import Home from "./components/pages/home"

class App extends Component {
  render() {
    return (
      <Provider values={stores.values} rules={stores.rules}>
        <Router>
          <Route path="/" component={Home} />
        </Router>
      </Provider>
    )
  }
}

export default App
