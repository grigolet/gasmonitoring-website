FROM node:8-alpine

ADD yarn.lock /yarn.lock
ADD package.json /package.json

ENV NODE_PATH=/node_modules
ENV PATH=$PATH:/node_modules/.bin
RUN yarn

RUN yarn add react-router-dom
RUN yarn add mobx mobx-react
RUN yarn add atmosphere.js

WORKDIR ./app
ADD . /app

EXPOSE 3000
EXPOSE 35729

RUN yarn build 
RUN npm i -g serve

USER 1001

CMD ["/usr/local/bin/serve", "-s", "build"]